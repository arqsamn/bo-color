@extends('layouts.theme')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">@isset($record) Ubah @else Tambah @endisset Non Aktifkan PO</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Non Aktifkan PO</a></li>
                        <li class="breadcrumb-item active">@isset($record) Ubah @else Tambah @endisset</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal" 
                        action="{{ isset($record) ? route('non_aktifkan_po.update', 1) : route('non_aktifkan_po.store') }}"
                        method="POST" novalidate>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="#">
                                            <div class="form-body">
                                                <h3 class="card-title">Tambah Data Barang Baru</h3>
                                                <hr>
                                                <!-- ROW 1 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">ID PO</label>
                                                            <input type="text" id="idnon_aktifkan_po" class="form-control" placeholder="ID non_aktifkan_po" autofocus>
                                                        </div>
                                                    </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Nama PO</label>
                                                                <input type="text" id="namanon_aktifkan_po" class="form-control" placeholder="Nama non_aktifkan_po" autofocus>
                                                            </div>
                                                        </div>
                                                </div>
                                                <!-- END ROW 1-->

                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection