@extends('layouts.theme')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Non Aktifkan PO (Purchase Order)</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Non Aktifkan PO</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <!-- Content Disini -->
                    <a href="{{ route('non_aktifkan_po.create') }}" class="btn btn-info waves-effect waves-light" type="button">
                        <span class="btn-label m-r-5"><i class="fas fa-plus"></i></span> Tambah
                    </a>

                    <div class="table-responsive m-t-20">
                        <table class="table table-bordered table-striped no-paddding dataTable">
                            <thead>
                            <tr>
                                    <th>No.</th>
                                    <th>ID PO</th>
                                    <th>Nama PO</th>
                                    <th>Tanggal PO</th>
                                    <th>Alasan Non Aktif</th>
                                    <th>nexttttt</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td>137793</td>
                                    <td>26 September 2022</td>
                                    <td>PO Panadol</td>
                                    <td>Stock masih banyak</td>
                                    <td>Data belum adaaaaa</td>
                                    <td>
                                        <a href="{{ route('non_aktifkan_po.edit', 1) }}" type="button" class="btn waves-effect btn-info btn-sm">
                                            <i class="ti-pencil-alt"></i> Ubah
                                        </a>
                                        <button type="button" data-id="1" class="btn waves-effect btn-danger btn-sm hapus">
                                            <i class="ti-trash"></i> Hapus
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection