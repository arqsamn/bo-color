@extends('layouts.theme')

@section('content')

<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li><a href="javascript:;">Page Options</a></li>
        <li class="active">Page with Top Menu</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Page with Top Menu <small>header small text goes here...</small></h1>
    <!-- end page-header -->
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Panel Title here</h4>
                </div>
                <div class="panel-body">
                    <!-- Content Disini -->
                    <form class="form-horizontal" 
                    action="{{ isset($record) ? route('corporate.update', 1) : route('corporate.store') }}"
                    method="POST" novalidate>

                    @isset($record)
                        @method('PUT')
                    @endisset

                    @csrf

                    <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="inputHorizontalSuccess" class="col-sm-4 col-form-label">Kode Corporate <span class="text-danger">*</span></label>
                            <div class="col-sm-6 controls">
                                <input type="text" name="kode_corporate" class="form-control"
                                    required data-validation-required-message="Harus Diisi">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputHorizontalSuccess" class="col-sm-4 col-form-label">Nama Corporate <span class="text-danger">*</span></label>
                            <div class="col-sm-8 controls">
                                <input type="text" name="nama_corporate" class="form-control"
                                    required data-validation-required-message="Harus Diisi">
                            </div>
                        </div>

                        <!-- Button -->
                        <button type="submit" class="btn waves-effect waves-light btn-info">
                            Simpan
                        </button>
                    </div>
                </form>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection