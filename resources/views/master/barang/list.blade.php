@extends('layouts.theme')

@section('content')

<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li><a href="javascript:;">Page Options</a></li>
        <li class="active">Page with Top Menu</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Page with Top Menu <small>header small text goes here...</small></h1>
    <!-- end page-header -->
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Master Barang</h4>
                </div>
                <div class="panel-body">
                    <!-- Content Disini -->
                    <a href="{{ route('barang.create') }}" class="btn btn-info waves-effect waves-light" type="button">
                        <span class="btn-label m-r-5"><i class="fas fa-plus"></i></span> Tambah
                    </a>

                    <div class="table-responsive m-t-20">
                        <table  id="data-table" class="table table-striped table-bordered display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>KD_BRG</th>
                                    <th>DP</th>
                                    <th>NM_BRG</th>
                                    <th>SATUAN</th>
                                    <th>ISI</th>
                                    <th>Q_AKHIR</th>
                                    <th>HJ_ECER</th>
                                    <th>STRIP</th>
                                    <th>Q_BBS</th>
                                    <th>HJ_BBS</th>
                                    <th>KL</th>
                                    <th>BUAT</th>
                                    <th>ID_FACTORY</th>
                                    <th>TGL_EXPIRED</th>
                                    <th>WKT_TOLERANSI</th>
                                    <th>TGL_REMIND</th>
                                    <th>KET_RETUR</th>
                                    <th>KONSINYASI</th>
                                    <th>TGL_KONSINYASI</th>
                                    <th>BARCODE</th>
                                    <th>KL</th>
                                    <th>WKT_TOLERANSI</th>
                                    <th>TGL_EXPIRED</th>
                                    <th>NEW_barang</th>
                                    <th>TGL_NEW_barang</th>
                                    <th>NILAI_PROMO</th>
                                    <th>TGL_AWAL_PROMO</th>
                                    <th>TGL_AKHIR_PROMO</th>
                                    <th>Q_AKHIR_REAL</th>
                                    <th>BRG_PUSDIS</th>
                                    <th>REMIND_PASIEN</th>
                                    <th>SAT_CAB</th>
                                    <th>MARK_UP</th>
                                    <th>HB_NET</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>0</td>
                                    <td>137793</td>
                                    <td>N4</td>
                                    <td>135Y PREMIUM C.BROWN HAIR 30 ML</td>
                                    <td>BOX</td>
                                    <td>20</td>
                                    <td>0</td>
                                    <td>11,800</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>11,800</td>
                                    <td>E</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>0</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>799439304079</td>
                                    <td>E</td>
                                    <td>0</td>
                                    <td>-</td>
                                    <td>N</td>
                                    <td>07/09/2017</td>
                                    <td>0</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>0,000</td>
                                    <td>TRUE</td>
                                    <td>-</td>
                                    <td>10</td>
                                    <td>27</td>
                                    <td>160000</td>
                                    <td>
                                        <a href="{{ route('barang.edit', 1) }}" type="button" class="btn waves-effect btn-info btn-sm">
                                            <i class="ti-pencil-alt"></i> Ubah
                                        </a>
                                        <button type="button" data-id="1" class="btn waves-effect btn-danger btn-sm hapus">
                                            <i class="ti-trash"></i> Hapus
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection