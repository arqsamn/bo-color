@extends('layouts.theme')

@section('content')

<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li><a href="javascript:;">Page Options</a></li>
        <li class="active">Page with Top Menu</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Page with Top Menu <small>header small text goes here...</small></h1>
    <!-- end page-header -->
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Master Supplier</h4>
                </div>
                <div class="panel-body">
                    <!-- Content Disini -->
                    <a href="{{ route('supplier.create') }}" class="btn btn-info waves-effect waves-light" type="button">
                        <span class="btn-label m-r-5"><i class="fas fa-plus"></i></span> Tambah
                    </a>

                    <div class="table-responsive m-t-20">
                        <table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ID Supplier</th>
                                    <th>Nama Supplier</th>
                                    <th>Alamat</th>
                                    <th>No. Telpon 1</th>
                                    <th>No. Telpon 2</th>
                                    <th>No. Telpon 3</th>
                                    <th>Email</th>
                                    <th>No. NPWP</th>
                                    <th>Nama NPWP</th>
                                    <th>Nama Contact Person</th>
                                    <th>No. Contact Person</th>
                                    <th>Nama Bank</th>
                                    <th>No. Rekening</th>
                                    <th>Nama Rekening</th>
                                    <th>Cabang Bank</th>
                                    <th>Created At</th>
                                    <th>Deleted Status</th>
                                    <th>Deleted At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td>123</td>
                                    <td>PT. ABC</td>
                                    <td>Bogor</td>
                                    <td>081212121212</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>abc@gmail.com</td>
                                    <td>029389398939</td>
                                    <td>Sukron</td>
                                    <td>Syakur</td>
                                    <td>081313131313</td>
                                    <td>Bank Mandiri</td>
                                    <td>23849038493849329</td>
                                    <td>Sukron</td>
                                    <td>Biak</td>
                                    <td>29-09-2022 09:01:23</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>
                                        <a href="{{ route('supplier.edit', 1) }}" type="button" class="btn waves-effect btn-info btn-sm">
                                            <i class="ti-pencil-alt"></i> Ubah
                                        </a>
                                        <button type="button" data-id="1" class="btn waves-effect btn-danger btn-sm hapus">
                                            <i class="ti-trash"></i> Hapus
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>122</td>
                                    <td>PT. ABCD</td>
                                    <td>Bogor</td>
                                    <td>081212121212</td>
                                    <td>082390300000</td>
                                    <td>-</td>
                                    <td>abcd@gmail.com</td>
                                    <td>029389398939</td>
                                    <td>Sukron</td>
                                    <td>Syakur</td>
                                    <td>081313131313</td>
                                    <td>Bank Mandiri</td>
                                    <td>23849038493849329</td>
                                    <td>Sukron</td>
                                    <td>Biak</td>
                                    <td>29-09-2022 09:01:23</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>
                                        <a href="{{ route('supplier.edit', 1) }}" type="button" class="btn waves-effect btn-info btn-sm">
                                            <i class="ti-pencil-alt"></i> Ubah
                                        </a>
                                        <button type="button" data-id="1" class="btn waves-effect btn-danger btn-sm hapus">
                                            <i class="ti-trash"></i> Hapus
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3.</td>
                                    <td>121</td>
                                    <td>PT. ABCDE</td>
                                    <td>Bogor</td>
                                    <td>081212121212</td>
                                    <td>-</td>
                                    <td>0834938493849</td>
                                    <td>abcde@gmail.com</td>
                                    <td>029389398939</td>
                                    <td>Sukron</td>
                                    <td>Syakur</td>
                                    <td>081313131313</td>
                                    <td>Bank Mandiri</td>
                                    <td>23849038493849329</td>
                                    <td>Sukron</td>
                                    <td>Biak</td>
                                    <td>29-09-2022 09:01:23</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>
                                        <a href="{{ route('supplier.edit', 1) }}" type="button" class="btn waves-effect btn-info btn-sm">
                                            <i class="ti-pencil-alt"></i> Ubah
                                        </a>
                                        <button type="button" data-id="1" class="btn waves-effect btn-danger btn-sm hapus">
                                            <i class="ti-trash"></i> Hapus
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection