@extends('layouts.theme')

@section('content')

<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li><a href="javascript:;">Page Options</a></li>
        <li class="active">Page with Top Menu</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Page with Top Menu <small>header small text goes here...</small></h1>
    <!-- end page-header -->
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Panel Title here</h4>
                </div>
                <div class="panel-body">
                    <!-- Content Disini -->
                    <form class="form-horizontal" 
                        action="{{ isset($record) ? route('supplier.update', 1) : route('supplier.store') }}"
                        method="POST" novalidate>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="#">
                                            <div class="form-body">
                                                <h3 class="card-title">Tambah Data Supplier</h3>
                                                <hr>
                                                <!-- ROW 1 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">ID Supplier</label>
                                                            <input type="text" id="idsupplier" class="form-control" placeholder="ID Supplier" autofocus>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Nama Supplier</label>
                                                            <input type="text" id="namasupplier" class="form-control" placeholder="Nama Supplier" autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END ROW 1-->
                                                
                                                <!-- ROW 2 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Alamat</label>
                                                            <input type="text" id="alamat" class="form-control" placeholder="Alamat" autofocus>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">No Telepon 1</label>
                                                            <input type="text" id="notelepon1 " class="form-control" placeholder="No Telepon 1" autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END ROW 2-->

                                                <!-- ROW 3 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">No Telepon 2</label>
                                                            <input type="text" id="notelepon2" class="form-control" placeholder="No Telepon 2" autofocus>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">No Telepon 3</label>
                                                            <input type="text" id="notelepon3 " class="form-control" placeholder="No Telepon 3" autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END ROW 3-->

                                                <!-- ROW 4 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Email</label>
                                                            <input type="text" id="email" class="form-control" placeholder="Email" autofocus>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">No NPWP</label>
                                                            <input type="text" id="nonpwp" class="form-control" placeholder="No NPWP" autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END ROW 4-->

                                                <!-- ROW 5 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Nama NPWP</label>
                                                            <input type="text" id="namanpwp" class="form-control" placeholder="Nama NPWP" autofocus>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Nama Contact Person</label>
                                                            <input type="text" id="namacontactperson" class="form-control" placeholder="Nama Contact Person" autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END ROW 5-->

                                                <!-- ROW 6 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">No Contact Person</label>
                                                            <input type="text" id="nocontactperson" class="form-control" placeholder="No Contact Person" autofocus>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Nama Bank</label>
                                                            <input type="text" id="namabank" class="form-control" placeholder="Nama Bank" autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END ROW 6-->

                                                <!-- ROW 6 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">No Rekening</label>
                                                            <input type="text" id="norekening" class="form-control" placeholder="No Rekening" autofocus>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Nama Rekening</label>
                                                            <input type="text" id="namarekening" class="form-control" placeholder="Nama Rekening" autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END ROW 6-->

                                                <!-- ROW 6 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Cabang Bank</label>
                                                            <input type="text" id="cabangbang" class="form-control" placeholder="Cabang Bank" autofocus>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Created At</label>
                                                            <input type="text" id="createdat" class="form-control" placeholder="Created At" autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END ROW 6-->
                                                
                                                <!-- ROW 6 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Deleted Status</label>
                                                            <input type="text" id="Deleted Status" class="form-control" placeholder="Deleted Status" autofocus>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Deleted At</label>
                                                            <input type="text" id="deletedat" class="form-control" placeholder="Deleted At" autofocus>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END ROW 6-->

                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection