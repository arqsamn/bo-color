@extends('layouts.theme')

@section('content')

<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li><a href="javascript:;">Page Options</a></li>
        <li class="active">Page with Top Menu</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Page with Top Menu <small>header small text goes here...</small></h1>
    <!-- end page-header -->
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Panel Title here</h4>
                </div>
                <div class="panel-body">
                    <!-- Content Disini -->
                    <form class="form-horizontal" 
                    action="{{ isset($record) ? route('cabang.update', 1) : route('cabang.store') }}"
                    method="POST" novalidate>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <form class="form-horizontal" 
                                    action="{{ isset($record) ? route('departement.update', 1) : route('departement.store') }}"
                                    method="POST" novalidate>
            
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <form action="#">
                                                        <div class="form-body">
                                                            <h3 class="card-title">Tambah Data Departement</h3>
                                                            <hr>
                                                            <!-- ROW 1 -->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">ID Departement</label>
                                                                        <input type="text" id="iddpepartement" class="form-control" placeholder="ID Departement" autofocus>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END ROW 1-->
            
                                                            <!-- ROW 2 -->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Nama Departement</label>
                                                                        <input type="text" id="namadepartement" class="form-control" placeholder="Nama Departement" autofocus>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END ROW 2-->
            
                                                        <div class="form-actions">
                                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                                            <button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection