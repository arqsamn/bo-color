@extends('layouts.theme')

@section('content')

<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li><a href="javascript:;">Page Options</a></li>
        <li class="active">Page with Top Menu</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Page with Top Menu <small>header small text goes here...</small></h1>
    <!-- end page-header -->
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Master User</h4>
                </div>
                <div class="panel-body">
                    <!-- Content Disini -->
                    <a href="{{ route('user.create') }}" class="btn btn-info waves-effect waves-light" type="button">
                        <span class="btn-label m-r-5"><i class="fas fa-plus"></i></span> Tambah
                    </a>

                    <div class="table-responsive m-t-20">
                        <table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>Password</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Alamat</th>
                                    <th>Created At</th>
                                    <th>Update At</th>
                                    <th>Status Delete</th>
                                    <th>Tanggal Delete</th>
                                    <th>ID Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>1</th>
                                    <th>666</th>
                                    <th>Jiung</th>
                                    <th>*****</th>
                                    <th>Laki-Laki</th>
                                    <th>Jakarta</th>
                                    <th>-</th>
                                    <th>Jakarta Pusat</th>
                                    <th>00:30, 24-09-2022</th>
                                    <th>00:30, 24-09-2022</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>11</th>
                                    <td>
                                        <a href="{{ route('user.edit', 1) }}" type="button" class="btn waves-effect btn-info btn-sm">
                                            <i class="ti-pencil-alt"></i> Ubah
                                        </a>
                                        <button type="button" data-id="1" class="btn waves-effect btn-danger btn-sm hapus">
                                            <i class="ti-trash"></i> Hapus
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection