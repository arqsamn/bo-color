@extends('layouts.theme')

@section('content')

<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li><a href="javascript:;">Page Options</a></li>
        <li class="active">Page with Top Menu</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Page with Top Menu <small>header small text goes here...</small></h1>
    <!-- end page-header -->
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Panel Title here</h4>
                </div>
                <div class="panel-body">
                    <!-- Content Disini -->
                    <form class="form-horizontal" 
                    action="{{ isset($record) ? route('user.update', 1) : route('user.store') }}"
                    method="POST" novalidate>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="#">
                                        <div class="form-body">
                                            <h3 class="card-title">Tambah Data User</h3>
                                            <hr>
                                            <!-- ROW 1 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">ID User</label>
                                                        <input type="text" id="iduser" class="form-control" placeholder="ID user" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Username</label>
                                                        <input type="text" id="username" class="form-control" placeholder="Username" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 1-->
                                            
                                            <!-- ROW 2 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nama Lengkap</label>
                                                        <input type="text" id="namalengkapt" class="form-control" placeholder="Nama Lengkap" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Password</label>
                                                        <input type="text" id="password" class="form-control" placeholder="Password" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 2-->

                                            <!-- ROW 3 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Jenis Kelamin</label>
                                                        <input type="text" id="jeniskelamin" class="form-control" placeholder="Jenis Kelamin" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Tanggal Lahir</label>
                                                        <input type="text" id="tanggallahir" class="form-control" placeholder="Tanggal Lahir" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 3-->

                                            <!-- ROW 4 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Alamat</label>
                                                        <input type="text" id="alamat" class="form-control" placeholder="Alamat" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Created At</label>
                                                        <input type="text" id="createdat" class="form-control" placeholder="Created At" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 4-->

                                            <!-- ROW 5 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Update At</label>
                                                        <input type="text" id="updateat" class="form-control" placeholder="Update At" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Status Delete</label>
                                                        <input type="text" id="namacontactperson" class="form-control" placeholder="Status Delete" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 5-->

                                            <!-- ROW 6 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Tanggal Delete</label>
                                                        <input type="text" id="tanggaldelete" class="form-control" placeholder="Tanggal Delete" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">ID Role</label>
                                                        <input type="text" id="idrole" class="form-control" placeholder="ID Role" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 6-->

                                            <!-- ROW 6 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">No Rekening</label>
                                                        <input type="text" id="norekening" class="form-control" placeholder="No Rekening" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nama Rekening</label>
                                                        <input type="text" id="namarekening" class="form-control" placeholder="Nama Rekening" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 6-->

                                            <!-- ROW 6 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Cabang Bank</label>
                                                        <input type="text" id="cabangbang" class="form-control" placeholder="Cabang Bank" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Created At</label>
                                                        <input type="text" id="createdat" class="form-control" placeholder="Created At" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 6-->
                                            
                                            <!-- ROW 6 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Deleted Status</label>
                                                        <input type="text" id="Deleted Status" class="form-control" placeholder="Deleted Status" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Deleted At</label>
                                                        <input type="text" id="deletedat" class="form-control" placeholder="Deleted At" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 6-->

                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                            <button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection