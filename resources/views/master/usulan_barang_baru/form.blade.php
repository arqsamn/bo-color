@extends('layouts.theme')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">@isset($record) Ubah @else Tambah @endisset Usulan Barang Baru</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Master</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Usulan Barang Baru</a></li>
                        <li class="breadcrumb-item active">@isset($record) Ubah @else Tambah @endisset</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal" 
                        action="{{ isset($record) ? route('usulan_barang_baru.update', 1) : route('usulan_barang_baru.store') }}"
                        method="POST" novalidate>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="#">
                                            <div class="form-body">
                                                <h3 class="card-title">Tambah Data Barang Baru</h3>
                                                <hr>
                                                <!-- ROW 1 -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">ID Barang Bary</label>
                                                            <input type="text" id="idusulan_barang_baru" class="form-control" placeholder="ID usulan_barang_baru" autofocus>
                                                        </div>
                                                    </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Nama Barang Baru</label>
                                                                <input type="text" id="namausulan_barang_baru" class="form-control" placeholder="Nama usulan_barang_baru" autofocus>
                                                            </div>
                                                        </div>
                                                </div>
                                                <!-- END ROW 1-->

                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection