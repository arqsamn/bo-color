@extends('layouts.theme')

@section('content')

<div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <form action="#">
                                    <div class="form-body">
                                        <h3 class="card-title">Tambah Data Karyawan</h3>
                                        <hr>
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group row">
                                                <label for="inputHorizontalSuccess" class="col-sm-4 col-form-label">ID Karyawan<span class="text-danger">*</span></label>
                                                <div class="col-sm-6 controls">
                                                    <input type="text" name="id_karyawan" class="form-control"
                                                        required data-validation-required-message="Harus Diisi">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputHorizontalSuccess" class="col-sm-4 col-form-label">Nama Karyawan<span class="text-danger">*</span></label>
                                                <div class="col-sm-8 controls">
                                                    <input type="text" name="nama_karyawan" class="form-control"
                                                        required data-validation-required-message="Harus Diisi">
                                                </div>
                                            </div>
                                <!-- Button -->
                            <button type="submit" class="btn waves-effect waves-light btn-info">
                                Simpan
                            </button>
                            </div>
                        </form>
                    </div>
                </div>

@endsection


@section('javascript')
<script>
</script>
@endsection