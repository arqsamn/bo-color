@extends('layouts.theme')

@section('content')

<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li><a href="javascript:;">Page Options</a></li>
        <li class="active">Page with Top Menu</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Page with Top Menu <small>header small text goes here...</small></h1>
    <!-- end page-header -->
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Panel Title here</h4>
                </div>
                <div class="panel-body">
                    <!-- Content Disini -->
                    <form class="form-horizontal" 
                    action="{{ isset($record) ? route('factory.update', 1) : route('factory.store') }}"
                    method="POST" novalidate>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="#">
                                        <div class="form-body">
                                            <h3 class="card-title">Tambah Data Factory</h3>
                                            <hr>
                                            <!-- ROW 1 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">ID Factory</label>
                                                        <input type="text" id="idFactory" class="form-control" placeholder="ID Factory" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nama Factory</label>
                                                        <input type="text" id="namafactory" class="form-control" placeholder="Nama Factory" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 1-->

                                            <!-- ROW 2 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Alamat Factory</label>
                                                        <input type="text" id="alamatFactory" class="form-control" placeholder="Alamat Factory" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">No Telepon</label>
                                                        <input type="text" id="notelepon" class="form-control" placeholder="No Telepon" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 2-->

                                            <!-- ROW 3 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Email</label>
                                                        <input type="text" id="email" class="form-control" placeholder="Email" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">No NPWP</label>
                                                        <input type="text" id="nonpwp" class="form-control" placeholder="No NPWP" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 3-->

                                            <!-- ROW 4 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Nama NPWP</label>
                                                        <input type="text" id="namanpwp" class="form-control" placeholder="Nama NPWP" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Alamat NPWP</label>
                                                        <input type="text" id="alamatnpwp" class="form-control" placeholder="Alamat NPWP" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 4-->

                                            <!-- ROW 5 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Created At</label>
                                                        <input type="text" id="createdat" class="form-control" placeholder="Created At" autofocus>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Deleted Status</label>
                                                        <input type="text" id="deletedstatus" class="form-control" placeholder="Deleted Status" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 5-->

                                            <!-- ROW 6 -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Deleted Status</label>
                                                        <input type="text" id="deletedstatus" class="form-control" placeholder="Deleted Status" autofocus>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END ROW 6-->

                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                            <button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection