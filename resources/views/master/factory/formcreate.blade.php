@extends('layouts.theme')

@section('content')

<div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <form action="#">
                                    <div class="form-body">
                                        <h3 class="card-title">Tambah Data Factory</h3>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">ID Factory</label>
                                                    <input type="text" id="idfactory" class="form-control" placeholder="Kode Factory" autofocus>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Nama Factory</label>
                                                    <input type="text" id="nmfactory" class="form-control" placeholder="Nama Factory">
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Alamat</label>
                                                    <input type="text" id="alamat" class="form-control" placeholder="Alamat Factory">
                                                </div>
                                            </div>
                                            <!--/span-->
                                           
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                        <label class="control-label">Email</label>
                                                        <input type="text" id="email" class="form-control" placeholder="Email">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                
                                                <!--/span-->
                                            
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">No. Telepon 1</label>
                                                <input type="text" id="notlp1" class="form-control" placeholder="Nomor Telepon (Wajib)">
                                            </div>
                                        </div>
                                        <!--/span-->
                                            <div class="col-md-6">
                                            <div class="form-group">
                                                    <label class="control-label">No. Telepon 2</label>
                                                    <input type="text" id="notlp2" class="form-control" placeholder="Nomor Telepon (Opsional)">
                                                </div>
                                            </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label class="control-label">No. Telepon 3</label>
                                                    <input type="text" id="notlp3" class="form-control" placeholder="Nomor Telepon (Opsional)">
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>


                                        <!--/row-->
                                        <h3 class="card-title m-t-40">Data NPWP</h3>
                                        <hr>
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>No. NPWP</label>
                                                    <input type="text" class="form-control" id="no-npwp">
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Nama NPWP</label>
                                                    <input type="text" class="form-control" id="nama-npwp">
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Nama Contact Person</label>
                                                    <input type="text" class="form-control" id="nama-cs">
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Nomor Contact Person</label>
                                                    <input type="text" class="form-control" id="no-cs">
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>

                                <!-- <div class="card-body">
                                <form action="#">
                                    <div class="form-body"> -->
                                        <h3 class="card-title m-t-40">Data Bank</h3>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Nama Bank</label>
                                                    <input type="text" id="firstName" class="form-control" placeholder="Nama Bank" autofocus>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">No. Rekening</label>
                                                    <input type="text" id="lastName" class="form-control" placeholder="Nomor Rekening">
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Nama Rekening</label>
                                                    <input type="text" id="lastName" class="form-control" placeholder="Nama Rekening">
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                            <div class="form-group">
                                                    <label class="control-label">Cabang Bank</label>
                                                    <input type="text" id="lastName" class="form-control" placeholder="Cabang Bank">
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        
                                        <h3 class="card-title m-t-40">Status</h3>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Created At</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Deleted Status</label>
                                                    <input type="text" class="form-control" id="deleted-status">
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Deleted At</label>
                                                    <input type="text" class="form-control" id="deleted-at">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                        <button type="button" class="btn btn-inverse">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

@endsection


@section('javascript')
<script>
</script>
@endsection