@extends('layouts.theme')

@section('content')

<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Invoice</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Invoice</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-body printableArea">
                            <!-- <h3><b>INVOICE</b> <span class="pull-right">#5669626</span></h3> -->
                            <!-- <hr> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left d-flex">
                                        <img src="{{ asset('assets/images/apotekroxy-logo.png') }}" alt="" height="70">
                                        <address>
                                            <h3> &nbsp;<b class="text-danger">AP. ROXY UJUNG HARAPAN (033)</b></h3>
                                            <p class="text-muted m-l-5">JL. Raya Ujung Harapan RT .09/015 Bekasi
                                                <br/> Tlp: 88381205</p>
                                        </address>
                                    </div>
                                    <div class="pull-right text-right">
                                        <address>
                                            <h4 class="font-bold text-center">REKAPITULASI OMSET PENJUALAN</h4>
                                            <div class="text-center">
                                                <p class="m-t-30 text-center d-inline pr-2"><b>Mulai Tanggal :</b> <i class="fa fa-calendar"></i> 23rd Jan 2017,</p>
                                                <p class="text-center d-inline"><b>Sampai Tanggal :</b> <i class="fa fa-calendar"></i> 25th Jan 2017</p>
                                            </div>
                                        </address>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive m-t-40" style="clear: both;">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Tanggal</th>
                                                    <th class="text-center">Penjualan Bruto</th>
                                                    <th class="text-center">Retur Penjualan</th>
                                                    <th class="text-center">Discount Penjualan</th>
                                                    <th class="text-center">Cash</th>
                                                    <th class="text-center">C-Card</th>
                                                    <th class="text-center">D-Card</th>
                                                    <th class="text-center">Piutang</th>
                                                    <th class="text-center">SMR</th>
                                                    <th class="text-center">Total Penjualan Netto</th>
                                                    <th class="text-center">DPP</th>
                                                    <th class="text-center">Resep</th>
                                                    <th class="text-center">OTC</th>
                                                    <th class="text-center">Tot Bon R</th>
                                                    <th class="text-center">Tot Bon S</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th class="text-center">25/09/2022</th>
                                                    <th class="text-center">19,885,380</th>
                                                    <th class="text-center">258,400</th>
                                                    <th class="text-center">104,625</th>
                                                    <th class="text-center">15,938,455</th>
                                                    <th class="text-center">835,200</th>
                                                    <th class="text-center">2,723,900</th>
                                                    <th class="text-center">24,800</th>
                                                    <th class="text-center">341,745</th>
                                                    <th class="text-center">19,522,355</th>
                                                    <th class="text-center">17,895,586</th>
                                                    <th class="text-center">13,322,470</th>
                                                    <th class="text-center">6,199,885</th>
                                                    <th class="text-center">159</th>
                                                    <th class="text-center">137</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">22/08/2022</th>
                                                    <th class="text-center">19,885,380</th>
                                                    <th class="text-center">258,400</th>
                                                    <th class="text-center">104,625</th>
                                                    <th class="text-center">15,938,455</th>
                                                    <th class="text-center">835,200</th>
                                                    <th class="text-center">2,723,900</th>
                                                    <th class="text-center">24,800</th>
                                                    <th class="text-center">341,745</th>
                                                    <th class="text-center">19,522,355</th>
                                                    <th class="text-center">17,895,586</th>
                                                    <th class="text-center">13,322,470</th>
                                                    <th class="text-center">6,199,885</th>
                                                    <th class="text-center">159</th>
                                                    <th class="text-center">137</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">17/03/2022</th>
                                                    <th class="text-center">19,885,380</th>
                                                    <th class="text-center">258,400</th>
                                                    <th class="text-center">104,625</th>
                                                    <th class="text-center">15,938,455</th>
                                                    <th class="text-center">835,200</th>
                                                    <th class="text-center">2,723,900</th>
                                                    <th class="text-center">24,800</th>
                                                    <th class="text-center">341,745</th>
                                                    <th class="text-center">19,522,355</th>
                                                    <th class="text-center">17,895,586</th>
                                                    <th class="text-center">13,322,470</th>
                                                    <th class="text-center">6,199,885</th>
                                                    <th class="text-center">159</th>
                                                    <th class="text-center">137</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">27/04/2022</th>
                                                    <th class="text-center">19,885,380</th>
                                                    <th class="text-center">258,400</th>
                                                    <th class="text-center">104,625</th>
                                                    <th class="text-center">15,938,455</th>
                                                    <th class="text-center">835,200</th>
                                                    <th class="text-center">2,723,900</th>
                                                    <th class="text-center">24,800</th>
                                                    <th class="text-center">341,745</th>
                                                    <th class="text-center">19,522,355</th>
                                                    <th class="text-center">17,895,586</th>
                                                    <th class="text-center">13,322,470</th>
                                                    <th class="text-center">6,199,885</th>
                                                    <th class="text-center">197</th>
                                                    <th class="text-center">167</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">21/06/2022</th>
                                                    <th class="text-center">19,885,380</th>
                                                    <th class="text-center">258,400</th>
                                                    <th class="text-center">104,625</th>
                                                    <th class="text-center">15,938,455</th>
                                                    <th class="text-center">835,200</th>
                                                    <th class="text-center">2,723,900</th>
                                                    <th class="text-center">24,800</th>
                                                    <th class="text-center">341,745</th>
                                                    <th class="text-center">19,522,355</th>
                                                    <th class="text-center">17,895,586</th>
                                                    <th class="text-center">13,322,470</th>
                                                    <th class="text-center">6,199,885</th>
                                                    <th class="text-center">188</th>
                                                    <th class="text-center">145</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Tombol disini -->
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <button id="print" class="btn btn-info btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
                                    </div>
                                </div>

                                <br>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>

@endsection

@section('javascript')
<script>
</script>
@endsection