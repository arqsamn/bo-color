@extends('layouts.theme')

@section('content')

<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Invoice</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Invoice</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-body printableArea">
                            <!-- <h3><b>INVOICE</b> <span class="pull-right">#5669626</span></h3> -->
                            <!-- <hr> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left d-flex">
                                        <img src="{{ asset('assets/images/apotekroxy-logo.png') }}" alt="" height="70">
                                        <address>
                                            <h3> &nbsp;<b class="text-danger">AP. ROXY UJUNG HARAPAN (033)</b></h3>
                                            <p class="text-muted m-l-5">JL. Raya Ujung Harapan RT .09/015 Bekasi
                                                <br/> Tlp: 88381205</p>
                                        </address>
                                    </div>
                                    <div class="pull-right text-right">
                                        <address>
                                            <h4 class="font-bold text-center">REKAPITULASI OMSET PENJUALAN KASIR</h4>
                                            <div class="text-center">
                                                <p class="m-t-30 text-center d-inline pr-2"><b>Mulai Tanggal :</b> <i class="fa fa-calendar"></i> 23rd Jan 2017,</p>
                                                <p class="text-center d-inline"><b>Sampai Tanggal :</b> <i class="fa fa-calendar"></i> 25th Jan 2017</p>
                                            </div>
                                        </address>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive m-t-40" style="clear: both;">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Kasir</th>
                                                    <th class="text-center">Penjualan Bruto</th>
                                                    <th class="text-center">Retur</th>
                                                    <th class="text-center">Discount</th>
                                                    <th class="text-center">Cash</th>
                                                    <th class="text-center">C-Card</th>
                                                    <th class="text-center">D-Card</th>
                                                    <th class="text-center">Piutang</th>
                                                    <th class="text-center">Tot SMR</th>
                                                    <th class="text-center">Tot Resep</th>
                                                    <th class="text-center">Tot Swalayan</th>
                                                    <th class="text-center">Penjualan</th>
                                                    <th class="text-center">Penjualan Netto</th>
                                                    <th class="text-center">Tot Bon R</th>
                                                    <th class="text-center">Tot Bon S</th>
                                                    <th class="text-center">Sblm Disc HC</th>
                                                    <th class="text-center">Ssdh Disc HC</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th class="text-center">2</th>
                                                    <th class="text-center">17,200</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center">17,200</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center">17,200</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">15,636</th>
                                                    <th class="text-center">17,200</th>
                                                    <th class="text-center">2</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center"> </th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">Andi</th>
                                                    <th class="text-center">2,366,300</th>
                                                    <th class="text-center">219,200</th>
                                                    <th class="text-center">11,160</th>
                                                    <th class="text-center"></th>1,912,440
                                                    <th class="text-center">0</th>
                                                    <th class="text-center">223,500</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center">12,060</th>
                                                    <th class="text-center">1,247,100</th>
                                                    <th class="text-center">888,840</th>
                                                    <th class="text-center">1,941,764</th>
                                                    <th class="text-center">2,135,940</th>
                                                    <th class="text-center">19</th>
                                                    <th class="text-center">12</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center"> </th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">Bayu</th>
                                                    <th class="text-center">2,366,300</th>
                                                    <th class="text-center">219,200</th>
                                                    <th class="text-center">11,160</th>
                                                    <th class="text-center"></th>1,912,440
                                                    <th class="text-center">0</th>
                                                    <th class="text-center">223,500</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center">12,060</th>
                                                    <th class="text-center">1,247,100</th>
                                                    <th class="text-center">888,840</th>
                                                    <th class="text-center">1,941,764</th>
                                                    <th class="text-center">2,135,940</th>
                                                    <th class="text-center">19</th>
                                                    <th class="text-center">12</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center"> </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Tombol disini -->
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <button id="print" class="btn btn-info btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
                                    </div>
                                </div>

                                <br>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>

@endsection

@section('javascript')
<script>
</script>
@endsection