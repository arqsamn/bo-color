@extends('layouts.theme')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Selisih Persediaan</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Selisih</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Persediaan</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <!-- Content Disini -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Mulai Tanggal</label>
                                <input type="date" class="form-control">
                                <label>Sampai Tanggal</label>
                                <input type="date" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Kategori</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio1">Barang Lebih</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio2">Barang Tertukar</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio3">Barang Kurang/Hilang/Rusak</label>
                                </div>
                                <label class="mt-3">Jenis Laporan</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio1">Ringkasan</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio2">Detail</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End -->
                    <div class="row col-md-3">
                        <div class="form-group mr-2">
                            <button type="submit" class="btn waves-effect waves-light btn-block btn-info">OK</button>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn waves-effect waves-light btn-block btn-primary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection