@extends('layouts.theme')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Laporan Kartu Hutang</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Laporan</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Kartu Hutang</a></li>
                        <li class="breadcrumb-item active">Pilih</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <!-- Content Disini -->
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Mulai Tanggal</label>
                                <input type="date" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Sampai Tanggal</label>
                                <input type="date" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nama Supplier</label>
                        <select class="custom-select col-3" id="inlineFormCustomSelect">
                            <option selected></option>
                        </select>
                    </div>
                    <div class="form-group ml-4">
                        <label>S/D  Supplier</label>
                        <select class="custom-select col-3" id="inlineFormCustomSelect">
                            <option selected></option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <label>Jenis Laporan</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio1">Tampilkan Semua Hutang</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                            <label class="custom-control-label" for="customRadio2">Tampilkan Hutang Yang Belum Lunas</label>
                        </div>
                    </div>
                    <div class="row col-md-3 mt-3">
                        <div class="form-group mr-2">
                            <a href="" class="btn waves-effect waves-light btn-block btn-info">OK</a>
                        </div>
                        <div class="form-group">
                            <a href="" class="btn waves-effect waves-light btn-block btn-primary">Batal</a>
                        </div>
                    </div>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection