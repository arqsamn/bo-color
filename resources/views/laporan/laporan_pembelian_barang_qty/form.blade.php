@extends('layouts.theme')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">@isset($record) Ubah @else Tambah @endisset Data </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Laporan</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Penjualan</a></li>
                        <li class="breadcrumb-item active">@isset($record) Ubah @else Tambah @endisset</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal" 
                        action="{{ isset($record) ? route('laporan_pembelian_barang_qty.update', 1) : route('laporan_pembelian_barang_qty.store') }}"
                        method="POST" novalidate>

                        @isset($record)
                            @method('PUT')
                        @endisset

                        @csrf

                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label for="inputHorizontalSuccess" class="col-sm-4 col-form-label">Kode laporan_pembelian_barang_qty <span class="text-danger">*</span></label>
                                <div class="col-sm-6 controls">
                                    <input type="text" name="kode_laporan_pembelian_barang_qty" class="form-control"
                                        required data-validation-required-message="Harus Diisi">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputHorizontalSuccess" class="col-sm-4 col-form-label">Nama laporan_pembelian_barang_qty <span class="text-danger">*</span></label>
                                <div class="col-sm-8 controls">
                                    <input type="text" name="nama_laporan_pembelian_barang_qty" class="form-control"
                                        required data-validation-required-message="Harus Diisi">
                                </div>
                            </div>

                            <!-- Button -->
                            <button type="submit" class="btn waves-effect waves-light btn-info">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection