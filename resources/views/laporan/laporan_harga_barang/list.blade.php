@extends('layouts.theme')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Laporan Harga Barang</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Laporan</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Harga Barang</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <!-- Content Disini -->
                    <div class="row">
                        <div class="col-md-3 mr-4">
                            <div class="form-group">
                                <label>Dari Kode Dept</label>
                                <input type="text" class="form-control">
                                <label>Sampai Kode Dept</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Dari Huruf Depan Brg</label>
                                <input type="text" class="form-control">
                                <label>Sampai Huruf Depan Brg</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio1">Urut Berdasarkan Abjad</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio2">Buat Label Harga Swalayan</label>
                    </div>
                    <!-- End -->
                    <div class="row col-md-3 mt-3">
                        <div class="form-group mr-2">
                            <button type="submit" class="btn waves-effect waves-light btn-block btn-info">OK</button>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn waves-effect waves-light btn-block btn-primary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection