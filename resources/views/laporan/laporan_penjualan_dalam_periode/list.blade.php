@extends('layouts.theme')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Laporan Penjualan Dalam Periode</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Laporan</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Penjualan Periode</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <!-- Content Disini -->
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Mulai Tanggal</label>
                                <input type="date" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Sampai Tanggal</label>
                                <input type="date" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- End -->
                    <div class="row col-md-3">
                        <div class="form-group mr-2">
                            <!-- <button type="submit" class="btn waves-effect waves-light btn-block btn-info">OK</button> -->
                            <a type="submit" class="btn waves-effect waves-light btn-block btn-info" href="laporan_penjualan_dalam_periode/print">OK</a>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn waves-effect waves-light btn-block btn-primary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection