@extends('layouts.theme')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Laporan Retur</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Laporan</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Retur</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <!-- Content Disini -->
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Periode</label>
                                <input type="date" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Sampai dengan</label>
                                <input type="date" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">Pilih Report</button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="javascript:void(0)">Ringkasan</a>
                            <a class="dropdown-item" href="javascript:void(0)">Detail</a>
                            <a class="dropdown-item" href="javascript:void(0)">Exit</a>
                        </div>
                    </div>      
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection