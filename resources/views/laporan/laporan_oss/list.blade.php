@extends('layouts.theme')

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Laporan OSS</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Laporan</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Pembelian</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <!-- Content Disini -->
                    {{-- <a href="{{ route('laporan_oss.create') }}" class="btn btn-info waves-effect waves-light" type="button">
                        <span class="btn-label m-r-5"><i class="fas fa-plus"></i></span> Tambah
                    </a> --}}

                    <div class="row">
                        <div class="col-lg-9 col-md-9">
                            <div class="table-responsive m-t-10">
                                <table class="table table-bordered table-striped no-paddding dataTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode Barang</th>
                                            <th>Nama Barang</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>
                                                <a href="{{ route('laporan_oss.edit', 1) }}" type="button" class="btn waves-effect btn-info btn-sm">
                                                    <i class="ti-pencil-alt"></i> Ubah
                                                </a>
                                                <button type="button" data-id="1" class="btn waves-effect btn-danger btn-sm hapus">
                                                    <i class="ti-trash"></i> Hapus
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="form-group">
                                <label class="control-label">Masukkan Tanggal Pembelian</label>
                                <input type="date" class="form-control" placeholder="dd/mm/yyyy">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nama Departemen</label>
                                <p>
                                    <select class="selectpicker" data-style="form-control btn-secondary">
                                    <option>Departemen 7</option>
                                    <option>Departemen 8</option>
                                    <option>Departemen 9</option>
                                    </select>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection