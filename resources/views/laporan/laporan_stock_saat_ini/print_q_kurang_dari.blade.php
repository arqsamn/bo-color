@extends('layouts.theme')

@section('content')

<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Invoice</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Invoice</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-body printableArea">
                            <!-- <h3><b>INVOICE</b> <span class="pull-right">#5669626</span></h3> -->
                            <!-- <hr> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left d-flex">
                                        <img src="{{ asset('assets/images/apotekroxy-logo.png') }}" alt="" height="70">
                                        <address>
                                            <h3> &nbsp;<b class="text-danger">AP. ROXY UJUNG HARAPAN (033)</b></h3>
                                            <p class="text-muted m-l-5">JL. Raya Ujung Harapan RT .09/015 Bekasi
                                                <br/> Tlp: 88381205</p>
                                        </address>
                                    </div>
                                    <div class="pull-right text-right">
                                        <address>
                                            <h4 class="font-bold text-center">LAPORAN STOCK s/d SAAT INI</h4>
                                            <h4 class="font-bold text-center">(SEMUA)</h4>
                                            <div class="text-center">
                                                <p class="m-t-30 text-center d-inline pr-2"><b>Tgl : 15/09/2022</b></p>
                                                <p class="m-t-30 text-center d-inline pr-2"><b>15:56:15</b></p>
                                            </div>
                                        </address>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="table-responsive m-t-40" style="clear: both;">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">KOD</th>
                                                    <th class="text-center">DP</th>
                                                    <th class="text-center">KL</th>
                                                    <th class="text-center">NM_BRGRDG</th>
                                                    <th class="text-center">HJ_ECER</th>
                                                    <th class="text-center">QTY</th>
                                                    <th class="text-center">KODE</th>
                                                    <th class="text-center">DP</th>
                                                    <th class="text-center">KL</th>
                                                    <th class="text-center">NM_BRGRDG</th>
                                                    <th class="text-center">HJ_ECER</th>
                                                    <th class="text-center">QTY</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th class="text-center">09977</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">A</th>
                                                    <th class="text-center">ADCAP 50CPS</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">07706</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">E</th>
                                                    <th class="text-center">COOLER MAT (COLD COMPRESS)</th>
                                                    <th class="text-center">7,500</th>
                                                    <th class="text-center">0</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">08203</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">A</th>
                                                    <th class="text-center">ADCAP 50CPS</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">07706</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">E</th>
                                                    <th class="text-center">COOLER MAT (COLD COMPRESS)</th>
                                                    <th class="text-center">7,500</th>
                                                    <th class="text-center">0</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">08189</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">A</th>
                                                    <th class="text-center">ADCAP 50CPS</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">07706</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">E</th>
                                                    <th class="text-center">COOLER MAT (COLD COMPRESS)</th>
                                                    <th class="text-center">7,500</th>
                                                    <th class="text-center">0</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">08186</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">A</th>
                                                    <th class="text-center">ADCAP 50CPS</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">07706</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">E</th>
                                                    <th class="text-center">COOLER MAT (COLD COMPRESS)</th>
                                                    <th class="text-center">7,500</th>
                                                    <th class="text-center">0</th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center">08187</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">A</th>
                                                    <th class="text-center">ADCAP 50CPS</th>
                                                    <th class="text-center">0</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">07706</th>
                                                    <th class="text-center"> </th>
                                                    <th class="text-center">E</th>
                                                    <th class="text-center">COOLER MAT (COLD COMPRESS)</th>
                                                    <th class="text-center">7,500</th>
                                                    <th class="text-center">0</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Tombol disini -->
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <button id="print" class="btn btn-info btn-outline" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
                                    </div>
                                </div>

                                <br>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>

@endsection

@section('javascript')
<script>
</script>
@endsection