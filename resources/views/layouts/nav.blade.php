<!-- begin #top-menu -->
<div id="top-menu" class="top-menu">
    <!-- begin top-menu nav -->
    <ul class="nav">
        {{-- Menu Display  --}}
        <li class="has-sub">
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="fa fa-laptop"></i>
                <span>Display</span>
            </a>
            <ul class="sub-menu">
                <li><a href="{{ url('usermanagement') }}">Usermanagement</a></li>
                <li><a href="{{ url('departementuser') }}">Departement</a></li>
            </ul>
        </li>
        {{-- MENU MASTER  --}}
        <li class="has-sub">
            <a href="javascript:;">
                <span class="badge pull-right">10</span>
                <i class="fa fa-inbox"></i> 
                <span>Master</span>
            </a>
            <ul class="sub-menu">
                <li><a href="{{ url('area') }}">Master Area</a></li>
                <li><a href="{{ url('cabang') }}">Master Cabang</a></li>
                <li><a href="{{ url('departement') }}">Master Departement</a></li>
                <li><a href="{{ url('kategori') }}">Master Kategori</a></li>
                <li><a href="{{ url('menu') }}">Master Menu</a></li>
                <li><a href="{{ url('factory') }}">Master Factory</a></li>
                <li><a href="{{ url('role') }}">Master Role</a></li>
                <li><a href="{{ url('satuan') }}">Master Satuan</a></li>
                <li><a href="{{ url('stokbarang') }}">Master Stok Barang</a></li>
                <li><a href="{{ url('supplier') }}">Master Supplier</a></li>
                <li><a href="{{ url('user') }}">Master User</a></li>
                <li><a href="{{ url('barang') }}">Master Barang</a></li>
                <li><a href="{{ url('karyawan') }}">Master Karyawan</a></li>
                <li><a href="{{ url('corporate') }}">Master Corporate</a></li>
                <li><a href="{{ url('dokter') }}">Master Dokter</a></li>
                <li><a href="{{ url('min_max') }}">Min - Max</a></li>
                <li><a href="{{ url('marketing_activity') }}">Marketing Activity</a></li>
                <li><a href="{{ url('non_aktifkan_po') }}">Non Aktifkan PO </a></li>
                <li><a href="{{ url('usulan_barang_baru') }}">Usulan Barang Baru</a></li>
                
            </ul>
        </li>

        {{-- Menu Laporan  --}}
        <li class="has-sub">
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="fa fa-clipboard"></i> 
                <span>Laporan</span>
            </a>
            <ul class="sub-menu">
                <li><a href="{{ url('laporan_penjualan') }}">Laporan Penjualan</a></li>
                <li><a href="{{ url('laporan_pembelian') }}">Laporan Pembelian</a></li>
                <li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        Laporan Selisih
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{ url('selisih_persediaan') }}">Selisih Persediaan</a></li>
                        <li><a href="{{ url('selisih_expired') }}">Selisih Expired</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('laporan_omset') }}">Laporan Omset</a></li>
                <li><a href="{{ url('laporan_cn_dn') }}">Laporan CN atau DN</a></li>
                <li><a href="{{ url('laporan_harga_barang') }}">Laporan Harga Barang</a></li>
                <li><a href="{{ url('laporan_penjualan_dalam_periode') }}">Laporan Penjualan Dalam Periode</a></li>
                <li><a href="{{ url('laporan_omset_penjualan_barang') }}">Laporan Omset Penjualan Barang</a></li>
                <li><a href="{{ url('laporan_penjualan_transaksi') }}">Laporan Penjualan By Tipe Transaksi</a></li>
                <li><a href="{{ url('laporan_penjualan_customer') }}">Laporan Penjualan By Jenis Customer</a></li>
                <li><a href="{{ url('laporan_penjualan_by_nama_dokter') }}">Laporan Penjualan Barang By Nama Dokter</a></li>
                <li><a href="{{ url('laporan_penjualan_by_customer') }}">Laporan Penjualan Barang By Customer</a></li>
                <li><a href="{{ url('laporan_pembelian_barang_qty') }}">Laporan Pembelian Barang QTY</a></li>
                <li><a href="{{ url('laporan_pembelian_barang') }}">Laporan Pembelian Barang</a></li>
                <li><a href="{{ url('laporan_purchase_order') }}">Laporan Bukti Purchase Order</a></li>
                <li><a href="{{ url('laporan_bukti_purchase') }}">Laporan Bukti Purchase Order Langsung</a></li>
                <li><a href="{{ url('resi_po_pembelian') }}">Resi PO-Pembelian</a></li>
                <li><a href="{{ url('laporan_input_barang_expired') }}">Laporan Input Barang Expired</a></li>
                <li><a href="{{ url('laporan_retur') }}">Laporan Retur</a></li>
                <li><a href="{{ url('laporan_delivery') }}">Laporan Delivery</a></li>
                <li><a href="{{ url('laporan_transfer_outlet') }}">Laporan Transfer Antar Outlet -- Summary</a></li>
                <li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        Laporan Utang Piutang
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{ url('laporan_kartu_hutang') }}">Laporan Kartu Hutang</a></li>
                        <li><a href="{{ url('laporan_kartu_piutang') }}">Laporan Kartu Piutang</a></li>
                        <li><a href="{{ url('laporan_kartu_piutangDepo') }}">Laporan Kartu Piutang DEPO</a></li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        Laporan OSS
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{ url('laporan_oss') }}">Laporan OSS</a></li>
                        <li><a href="app-email-detail.html">Laporan Barang Tidak Bergerak</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('laporan_stock_saat_ini') }}">Laporan Stok Saat Ini</a></li>
                <li><a href="{{ url('laporan_notasi_barang') }}">Laporan Notasi Barang</a></li>
                <li><a href="{{ url('laporan_perhitungan_hpp_perabjad') }}">Laporan Perhitungan HPP Per Abjad</a></li>
                <li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        Laporan Narkotika
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{ url('tabel_info_narkotika') }}">Table Info Laporan Narkotika</a></li>
                        <li><a href="{{ url('laporan_narkotika') }}">Laporan Narkotika</a></li>
                        <li><a href="{{ url('laporan_narkotika_psikotoprika') }}">Laporan Narkotika dan Psikotoprika</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('laporan_nilai_penjualan_perjam') }}">Laporan Nilai Penjualan Per jam</a></li>
                <li><a href="{{ url('laporan_pareto_penjualan') }}">Laporan Pareto Penjualan</a></li>
                <li><a href="{{ url('laporan_penjualan_tertentu') }}">Laporan Penjualan Barang Tertentu</a></li>
                <li><a href="{{ url('laporan_qty_jual_perjam') }}">Laporan QTY Jual Per Jam</a></li>
                <li><a href="{{ url('laporan_stock_summary') }}">Laporan Stock Produk Summary</a></li>
                <li><a href="{{ url('laporan_stock_kosong') }}">Laporan Stock Kosong</a></li>
                <li><a href="{{ url('laporan_nilai_inventory') }}">Laporan Nilai Inventory By Periode</a></li>
                <li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        Laporan Kepuasan Pelanggan
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{ url('rekap') }}">Rekap</a></li>
                        <li><a href="{{ url('detail') }}">Detail</a></li>
                    </ul>
                </li>
                <li><a href="{{ url('laporan_antrian_resep') }}">Laporan Antrian Resep</a></li>
                <li><a href="{{ url('laporan_konstribusi_departemen') }}">Laporan Kontribusi Departemen</a></li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="fa fa-shopping-cart"></i>
                <span>Transaksi</span> 
            </a>
            <ul class="sub-menu">
                <li class="has-sub">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        Transaksi Persediaan
                    </a>
                    <ul class="sub-menu">
                        <li><a href="app-email.html">Pembelian</a></li>
                        <li><a href="app-email.html">Return CN Pembelian</a></li>
                        <li><a href="app-email.html">Selisih Persediaan</a></li>
                        <li><a href="app-email.html">Selisih Expired</a></li>
                        <li><a href="app-email.html">Transfer Depo</a></li>
                        <li><a href="app-email.html">Return Transfer Depo</a></li>
                        <li><a href="app-email.html">Transfer Barang Antar Outlet</a></li>
                        <li><a href="app-email.html">CN Transfer Barang Antar Outlet</a></li>
                        <li><a href="app-email.html">Terima Transfer Barang Antar Outlet</a></li>
                        <li><a href="app-email.html">CN Terima Transfer Barang Antar Outlet</a></li>
                        <li><a href="app-email.html">Input Barang Lebih Racikan</a></li>
                    </ul>
                </li>
                <li><a href="app-chat.html">Perubahan Harga</a></li>
                <li><a href="app-contact.html">Item Promo</a></li>
                <li><a href="app-contact2.html">Pelunasan Hutang</a></li>
                <li><a href="app-contact-detail.html">Pelunasan Piutang</a></li>
                <li><a href="app-contact-detail.html">Pelunasan Hutang System Lama</a></li>
                <li><a href="app-contact-detail.html">Pelunasan Piutang System Lama</a></li>
                <li><a href="app-contact-detail.html">Pelunasan Piutang Depo</a></li>
                <li><a href="app-contact-detail.html">Pelunasan Piutang Depo System Lama</a></li>
                <li><a href="app-contact-detail.html">Kalkulasi PO</a></li>
                <li><a href="app-contact-detail.html">Purchase Order</a></li>
                <li><a href="app-contact-detail.html">Purchase Order [CITO]</a></li>
                <li><a href="app-contact-detail.html">Proses Kirim Data PO ke Pusdis</a></li>
                <li><a href="app-contact-detail.html">Proses Terima Data Dari Pusdis</a></li>
                <li><a href="app-contact-detail.html">Kirim Data Depo</a></li>
                <li><a href="app-contact-detail.html">Terima Data Depo</a></li>
                <li><a href="app-contact-detail.html">Proses Data Depo</a></li>
                <li><a href="app-contact-detail.html">Purchase Order [CITO DEPO]</a></li>
                <li><a href="app-contact-detail.html">Laporan Transfer Depo</a></li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;">
                <b class="caret pull-righ"></b>
                <i class="fa fa-suitcase"></i>
                <span>Apotek</span> 
            </a>
            <ul class="sub-menu">
                <li><a href="form-basic.html">POS Apotek</a></li>
                <li><a href="form-layout.html">Monitoring Transaksi</a></li>
                <li><a href="form-addons.html">Penyelesaian Resep</a></li>
                <li><a href="form-material.html">E tiket</a></li>
                <li><a href="form-float-input.html">Jasa Dokter</a></li>
                <li><a href="form-pickers.html">Input dan Expired</a></li>
                <li><a href="form-upload.html">Penjualan Harian</a></li>
                <li><a href="form-mask.html">Laporan Penjualan Harian</a></li>
                <li><a href="form-validation.html">Laporan Voucher</a></li>
                <li><a href="{{ ('rekapitulasi_omset_penjualan') }}">Rekapitulasi Omset Penjualan</a></li>
                <li><a href="form-dropzone.html">Rekapitulasi Omset Penjualan Kasir</a></li>
                <li><a href="form-icheck.html">Rekapitulasi Omset (Piutang)</a></li>
                <li><a href="form-img-cropper.html">Rekapitulasi Omset Penjualan Per Shift</a></li>
                <li><a href="form-bootstrapwysihtml5.html">Laporan Penjualan Detail</a></li>
                <li><a href="form-typehead.html">Laporan Pandemic Care</a></li>
                <li><a href="form-wizard.html">Laporan Omset Jenis Barang</a></li>
                <li><a href="form-xeditable.html">Laporan Omset Ethical Resep - Non resep - OTC</a></li>
                <li><a href="form-summernote.html">Laporan Rekap Respon</a></li>
                <li><a href="form-tinymce.html">Laporan Omset Promo</a></li>
                <li><a href="form-tinymce.html">Laporan Up selling</a></li>
                <li><a href="form-tinymce.html">Jadwal Jaga</a></li>
                <li><a href="form-tinymce.html">Laporan Log Book Resep</a></li>
                <li><a href="form-tinymce.html">Postingan Harian</a></li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="fa fa-gear"></i>
                <span>Setup</span>
            </a>
            <ul class="sub-menu">
                <li><a href="form-basic.html">Parameter</a></li>
                <li><a href="form-layout.html">Setup Back Office</a></li>
                <li><a href="form-addons.html">Setup User</a></li>
                <li><a href="form-material.html">Setup E tiket</a></li>
                <li><a href="form-float-input.html">Setup Printer E Tiket</a></li>
                <li><a href="form-pickers.html">Setup Kasir</a></li>
                <li><a href="form-upload.html">Setup Antrian</a></li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="fa fa-area-chart"></i>
                <span>Maintenance</span>
            </a>
            <ul class="sub-menu">
                <li><a href="chart-flot.html">Stok Main</a></li>
            </ul>
        </li>
        <li class="has-sub">
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="fa fa-area-chart"></i>
                <span>Audit</span>
            </a>
            <ul class="sub-menu">
                <li><a href="form-basic.html">Laporan Diskon Penjualan</a></li>
                <li><a href="form-layout.html">Laporan Koreksi Penjualan</a></li>
                <li><a href="form-addons.html">Form Stock Opname</a></li>
            </ul>
        </li>
        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><span class="hide-menu">Manager</span></a>
        </li>
        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><span class="hide-menu">Remind Pasien</span></a>
        </li>
        <li class="has-sub">
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="fa fa-area-chart"></i>
                <span>Transfer Data</span>
            </a>
            <ul class="sub-menu">
                <li><a href="form-basic.html">Penjualan & Pembelian Pusdis</a></li>
                <li><a href="form-layout.html">Pembelian Langsung</a></li>
            </ul>
        </li>
        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><span class="hide-menu">Service Level</span></a>
        </li>
        <li class="has-sub">
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="fa fa-key"></i>
                <span>Login & Register</span>
            </a>
            <ul class="sub-menu">
                <li><a href="login.html">Login</a></li>
                <li><a href="login_v2.html">Login v2</a></li>
                <li><a href="login_v3.html">Login v3</a></li>
                <li><a href="register_v3.html">Register v3</a></li>
            </ul>
        </li>
        <li class="menu-control menu-control-left">
            <a href="#" data-click="prev-menu"><i class="fa fa-angle-left"></i></a>
        </li>
        <li class="menu-control menu-control-right">
            <a href="#" data-click="next-menu"><i class="fa fa-angle-right"></i></a>
        </li>
    </ul>
    <!-- end top-menu nav -->
</div>
<!-- end #top-menu -->