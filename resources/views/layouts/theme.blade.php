<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.png') }}">
    <title>Apote Roxy | Dashboard</title>

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="{{ asset('assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/style.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/style-responsive.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/theme/default.css') }}" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <link rel="stylesheet" href="{{ asset('assets/swetalert2/sweetalert2.min.css') }}">
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="{{ asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css') }}" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->
	
	



	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>

    
    {{-- ELITE ADMIN START --}}

    <style>
        .dataTables_wrapper{
            padding: 0px 0px !important;
        }
    </style>
</head>

<body class="">
    <div id="page-container" class="page-container fade page-without-sidebar page-header-fixed">
        @include('layouts.header')
        @include('layouts.nav')
        @yield('content')

    </div>
    {{-- <div id="content" class="content">
        <header class="topbar">
            @include('layouts.header')
		</header>
        <aside class="left-sidebar">
            @include('layouts.nav')
        </aside>
        <div class="page-wrapper">
            @yield('content')
        </div>
        <footer class="footer">
        </footer>
    </div> --}}


 
    {{-- <script src="{{ asset('assets/node_modules/popper/popper.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script> --}}
{{-- 
    <script src="{{ asset('dist/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('dist/js/pages/jquery.PrintArea.js') }}" type="text/JavaScript"></script>
    <script src="{{ asset('dist/js/waves.js') }}"></script>
    <script src="{{ asset('dist/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('dist/js/custom.min.js') }}"></script>
    
    <script src="{{ asset('assets/node_modules/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script> 
    
    <script src="{{ asset('assets/node_modules/raphael/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/morrisjs/morris.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/toast-master/js/jquery.toast.js') }}"></script>
    <script src="{{ asset('dist/js/dashboard1.js') }}"></script> --}}

    {{-- Elite Admin  --}}
    {{-- <script> 
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script> --}}
    {{-- <script>
        $(function(){
            $('.dataTable').DataTable();
        });
    </script> --}}

    
    <!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ asset('assets/plugins/pace/pace.min.js') }}"></script>
	<!-- ================== END BASE JS ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ asset('assets/plugins/jquery/jquery-1.9.1.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/jquery/jquery-migrate-1.1.0.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="{{ asset('assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/jquery-cookie/jquery.cookie.js') }}"></script>
    
    <script src="{{ asset('assets/swetalert2/sweetalert2.min.js') }}"></script>
    {{-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> --}}
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="{{ asset('assets/js/apps.min.js') }}"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->

    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="{{ asset('assets/plugins/DataTables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('assets/js/table-manage-responsive.demo.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/js/table-manage-scroller.demo.min.js') }}"></script> --}}
	<script src="{{ asset('assets/js/apps.min.js') }}"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->

    {{-- <script src="{{ asset('assets/node_modules/jquery/jquery-3.2.1.min.js') }}"></script> --}}
    <script>
		$(document).ready(function() {
			App.init();
			TableManageResponsive.init();
		});
	</script>
     <script>
        $(document).ready(function() {
            $("#print").click(function() {
                var mode = 'iframe'; //popup
                var close = mode == "popup";
                var options = {
                    mode: mode,
                    popClose: close
                };
                $("div.printableArea").printArea(options);
            });
        });
        
        $(function(){
            $('.dataTable').DataTable();
        });

        // Deleted Data
        $(".hapus").click(function(){
            console.log('masukk')
            
            let token = $('#token').val()
            let url = $('#baseUrl').val()
            let id = $(this).data("id");

            Swal.fire({
                text: "Mau menghapus data ini ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#038fcd',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url + '/' + id,
                        type: 'DELETE',
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        success: function (resp){
                            if (resp.status == 'success') {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'Data Terhapus!'
                                }).then((result) => {
                                    location.reload();
                                })
                            }
                        }
                    });
                }
            })
        });

        // Alert
        let alertSuccess = '{!! Session::get('alert-success') !!}';
        let alertError = '{!! Session::get('alert-error') !!}';

        if(alertSuccess != ''){
            showAlertSuccess()
        }else if(alertError != ''){
            showAlertError()
        }

        function showAlertSuccess() {
            Swal.fire("Berhasil", "Data Tersimpan!", "success");
        }

        function showAlertError() {
            Swal.fire("Maaf", "Data Gagal Tersimpan!", "error");
        }
    </script>
</body>

</html>