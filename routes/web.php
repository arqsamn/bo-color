<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\HomeController;
// use App\Http\Controllers\ProductController;
use App\Http\Controllers\UsermanagementController;
use App\Http\Controllers\DepartementuserController;


// MENU MASTER
use App\Http\Controllers\Master\BarangController;
use App\Http\Controllers\Master\CabangController;
use App\Http\Controllers\Master\SupplierController;
use App\Http\Controllers\Master\FactoryController;
use App\Http\Controllers\Master\DepartementController;
use App\Http\Controllers\Master\CorporateController;
use App\Http\Controllers\Master\DokterController;
use App\Http\Controllers\Master\MarketingActivityController;
use App\Http\Controllers\Master\BarangBaruController;
use App\Http\Controllers\Master\MinMaxController;
use App\Http\Controllers\Master\NonAktifkanPOController;
use App\Http\Controllers\Master\KategoriController;
use App\Http\Controllers\Master\KaryawanController;
use App\Http\Controllers\Master\MenuController;
use App\Http\Controllers\Master\UserController;
use App\Http\Controllers\Master\AreaController;
use App\Http\Controllers\Master\RoleController;
use App\Http\Controllers\Master\SatuanController;
use App\Http\Controllers\Master\StokBarangController;


// MENU LAPORAN
use App\Http\Controllers\Laporan\LaporanPenjualanController;
use App\Http\Controllers\Laporan\LaporanPembelianController;
use App\Http\Controllers\Laporan\SelisihPersediaanController;
use App\Http\Controllers\Laporan\SelisihExpiredController;
use App\Http\Controllers\Laporan\LaporanOmsetController;
use App\Http\Controllers\Laporan\LaporanCNDNController;
use App\Http\Controllers\Laporan\LaporanHargaBarangController;
use App\Http\Controllers\Laporan\LaporanPenjualanDalamPeriodeController;
use App\Http\Controllers\Laporan\LaporanOmsetPenjualanBarangController;
use App\Http\Controllers\Laporan\LaporanPenjualanByTipeTransaksiController;
use App\Http\Controllers\Laporan\LaporanPenjualanByJenisCustomerController;
use App\Http\Controllers\Laporan\LaporanPenjualanBarangByNamaDokterController;
use App\Http\Controllers\Laporan\LaporanPenjualanBarangByCustomerController;
use App\Http\Controllers\Laporan\LaporanPembelianBarangQTYController;
use App\Http\Controllers\Laporan\LaporanPembelianBarangController;
use App\Http\Controllers\Laporan\LaporanBuktiPurchaseOrderController;
use App\Http\Controllers\Laporan\LaporanBuktiPurchaseOrderLangsungController;
use App\Http\Controllers\Laporan\ResiPOPembelianController;
use App\Http\Controllers\Laporan\LaporanInputBarangExpiredController;
use App\Http\Controllers\Laporan\LaporanReturController;
use App\Http\Controllers\Laporan\LaporanDeliveryController;
use App\Http\Controllers\Laporan\LaporanTransferAntarOutletSummaryController;
use App\Http\Controllers\Laporan\LaporanKartuHutangController;
use App\Http\Controllers\Laporan\LaporanKartuPiutangController;
use App\Http\Controllers\Laporan\LaporanKartuPiutangDepoController;
use App\Http\Controllers\Laporan\LaporanOSSController;
use App\Http\Controllers\Laporan\LaporanStockSaatIniController;
use App\Http\Controllers\Laporan\LaporanNotasiBarangController;
use App\Http\Controllers\Laporan\LaporanPerhitunganHPPPerAbjadController;
use App\Http\Controllers\Laporan\TabelInfoNarkotikaController;
use App\Http\Controllers\Laporan\LaporanNarkotikaController;
use App\Http\Controllers\Laporan\LaporanNarkotikadanPsikotoprikaController;
use App\Http\Controllers\Laporan\LaporanNilaiPenjualanPerJamController;
use App\Http\Controllers\Laporan\LaporanParetoPenjualanController;
use App\Http\Controllers\Laporan\LaporanPenjualanBarangTertentuController;
use App\Http\Controllers\Laporan\LaporanQTYJualPerJamController;
use App\Http\Controllers\Laporan\LaporanStockProdukSummaryController;
use App\Http\Controllers\Laporan\LaporanStockKosongController;
use App\Http\Controllers\Laporan\LaporanNilaiInventoryByPeriodeController;
use App\Http\Controllers\Laporan\RekapController;
use App\Http\Controllers\Laporan\DetailController;
use App\Http\Controllers\Laporan\LaporanAntrianResepController;
use App\Http\Controllers\Laporan\LaporanKonstribusiDepartemenController;

// MENU TRANSAKSI

// MENU APOTEK

// MENU SETUP

// MENU MAINTENANCE

// MENU AUDIT

// MENU MANAGER

// MENU REMIND PASIEN

// MENU TRANSFER DATA

// MENU SERVICE LEVEL

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'index']);
Route::resource('home', HomeController::class);
Route::get('login', [LoginController::class, 'index'])->name('login');
Route::get('logout', [LoginController::class, 'logout']);

Route::post('auth/login', [LoginController::class, 'login'])->name('auth.login');

Route::group(['middleware' => 'cekauth'], function () {
	// MENU DISPLAY
	Route::resource('usermanagement', UsermanagementController::class);
	// Route::resource('product', ProductController::class);
	Route::resource('departementuser', DepartementuserController::class);

	// MENU MASTER
	Route::resource('barang', BarangController::class);
	Route::resource('cabang', CabangController::class);
	Route::resource('supplier', SupplierController::class);
	Route::resource('factory', FactoryController::class);
	Route::resource('departement', DepartementController::class);
	Route::resource('corporate', CorporateController::class);
	Route::resource('dokter', DokterController::class);
	Route::resource('marketing_activity', MarketingActivityController::class);
	Route::resource('usulan_barang_baru', BarangBaruController::class);
	Route::resource('min_max', MinMaxController::class);
	Route::resource('non_aktifkan_po', NonAktifkanPOController::class);
	Route::resource('kategori', KategoriController::class);
	Route::resource('karyawan', KaryawanController::class);
	Route::resource('menu', MenuController::class);
	Route::resource('user', UserController::class);
	Route::resource('area', AreaController::class);
	Route::resource('role', RoleController::class);
	Route::resource('satuan', SatuanController::class);
	Route::resource('stokbarang', StokBarangController::class);


	// MENU LAPORAN

	// Laporan Penjualan
	Route::get('laporan_penjualan/print', [LaporanPenjualanController::class,'print']);
	Route::resource('laporan_penjualan', LaporanPenjualanController::class);
	//

	// Laporan Penjualan dalam Periode
	Route::get('laporan_penjualan_dalam_periode/print', [LaporanPenjualanDalamPeriodeController::class, 'print']);
	Route::resource('laporan_penjualan_dalam_periode', LaporanPenjualanDalamPeriodeController::class);
	//

	// Laporan Omzet Penjualan Barang
	Route::get('laporan_omset_penjualan_barang/print', [LaporanOmsetPenjualanBarangController::class, 'print']);
	Route::resource('laporan_omset_penjualan_barang', LaporanOmsetPenjualanBarangController::class);

	// Laporan Penjualan By Type Transaksi
	Route::get('laporan_penjualan_transaksi/print_resep', [LaporanPenjualanByTipeTransaksiController::class,'print_resep']);
	Route::get('laporan_penjualan_transaksi/print_swalayan', [LaporanPenjualanByTipeTransaksiController::class,'print_swalayan']);
	Route::resource('laporan_penjualan_transaksi', LaporanPenjualanByTipeTransaksiController::class);

	// Laporan Pembelian Barang QTY
	Route::get('laporan_pembelian_barang_qty/print', [LaporanPembelianBarangQTYController::class, 'print']);
	Route::resource('laporan_pembelian_barang_qty', LaporanPembelianBarangQTYController::class);

	Route::get('laporan_penjualan_by_nama_dokter/per_dokter', [LaporanPenjualanBarangByNamaDokterController::class, 'per_dokter']);
	Route::get('laporan_penjualan_by_nama_dokter/all_dokter', [LaporanPenjualanBarangByNamaDokterController::class, 'all_dokter']);
	Route::get('laporan_penjualan_by_nama_dokter/rekap_omset', [LaporanPenjualanBarangByNamaDokterController::class, 'rekap_omset']);
	Route::get('laporan_penjualan_by_nama_dokter/rekap_sc', [LaporanPenjualanBarangByNamaDokterController::class, 'rekap_sc']);
	Route::resource('laporan_penjualan_by_nama_dokter', LaporanPenjualanBarangByNamaDokterController::class);

	Route::resource('laporan_pembelian', LaporanPembelianController::class);
	Route::resource('selisih_persediaan', SelisihPersediaanController::class);
	Route::resource('selisih_expired', SelisihExpiredController::class);
	Route::resource('laporan_omset', LaporanOmsetController::class);
	Route::resource('laporan_cn_dn', LaporanCNDNController::class);
	Route::resource('laporan_harga_barang', LaporanHargaBarangController::class);

	Route::resource('laporan_penjualan_customer', LaporanPenjualanByJenisCustomerController::class);
	Route::resource('laporan_penjualan_by_customer', LaporanPenjualanBarangByCustomerController::class);
	Route::resource('laporan_pembelian_barang', LaporanPembelianBarangController::class);
	Route::resource('laporan_purchase_order', LaporanBuktiPurchaseOrderController::class);
	Route::resource('laporan_bukti_purchase', LaporanBuktiPurchaseOrderLangsungController::class);
	Route::resource('resi_po_pembelian', ResiPOPembelianController::class);
	Route::resource('laporan_input_barang_expired', LaporanInputBarangExpiredController::class);
	Route::resource('laporan_retur', LaporanReturController::class);
	Route::resource('laporan_delivery', LaporanDeliveryController::class);
	Route::resource('laporan_transfer_outlet', LaporanTransferAntarOutletSummaryController::class);
	Route::resource('laporan_kartu_hutang', LaporanKartuHutangController::class);
	Route::resource('laporan_kartu_piutang', LaporanKartuPiutangController::class);
	Route::resource('laporan_kartu_piutangDepo', LaporanKartuPiutangDepoController::class);
	Route::resource('laporan_oss', LaporanOSSController::class);
	Route::resource('laporan_stock_saat_ini', LaporanStockSaatIniController::class);
	Route::resource('laporan_notasi_barang', LaporanNotasiBarangController::class);
	Route::resource('laporan_perhitungan_hpp_perabjad', LaporanPerhitunganHPPPerAbjadController::class);
	Route::resource('tabel_info_narkotika', TabelInfoNarkotikaController::class);
	Route::resource('laporan_narkotika', LaporanNarkotikaController::class);
	Route::resource('laporan_narkotika_psikotoprika', LaporanNarkotikadanPsikotoprikaController::class);
	Route::resource('laporan_nilai_penjualan_perjam', LaporanNilaiPenjualanPerJamController::class);
	Route::resource('laporan_pareto_penjualan', LaporanParetoPenjualanController::class);
	Route::resource('laporan_penjualan_tertentu', LaporanPenjualanBarangTertentuController::class);
	Route::resource('laporan_qty_jual_perjam', LaporanQTYJualPerJamController::class);
	Route::resource('laporan_stock_summary', LaporanStockProdukSummaryController::class);
	Route::resource('laporan_stock_kosong', LaporanStockKosongController::class);
	Route::resource('laporan_nilai_inventory', LaporanNilaiInventoryByPeriodeController::class);
	Route::resource('rekap', RekapController::class);
	Route::resource('detail', DetailController::class);
	Route::resource('laporan_antrian_resep', LaporanAntrianResepController::class);
	Route::resource('laporan_konstribusi_departemen', LaporanKonstribusiDepartemenController::class);

	// MENU TRANSAKSI

	// MENU APOTEK

	// MENU SETUP

	// MENU MAINTENANCE

	// MENU AUDIT

	// MENU MANAGER

	// MENU REMIND PASIEN

	// MENU TRANSFER DATA

	// MENU SERVICE LEVEL
});
