<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class BarangController extends Controller
{
    public function index()
    {
        return view('master.barang.list');
    }

    public function create()
    {
        return view('master.barang.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('master.barang.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
