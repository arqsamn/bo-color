<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LogoutController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function destroy($id): JsonResponse
    {
    }
}
