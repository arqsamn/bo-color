<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class RoleController extends Controller
{
    public function index()
    {
        return view('master.role.list');
    }

    public function create()
    {
        return view('master.role.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('master.role.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
