<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class KaryawanController extends Controller
{
    public function index()
    {
        return view('master.karyawan.list');
    }

    public function create()
    {
        return view('master.karyawan.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('master.karyawan.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
