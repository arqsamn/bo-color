<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class NonAktifkanPOController extends Controller
{
    public function index()
    {
        return view('master.nonaktifPO.list');
    }

    public function create()
    {
        return view('master.nonaktifPO.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('master.nonaktifPO.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
