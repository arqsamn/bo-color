<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class DokterController extends Controller
{
    public function index()
    {

        // $token = Session::get('access_token');
        // exit;
        $client = new Client();
        $url = "https://api.app-sandbox.xyz/api/dokter/all";
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLmFwcC1zYW5kYm94Lnh5elwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY2NDA3ODMwNSwiZXhwIjoxNjY2NjcwMzA1LCJuYmYiOjE2NjQwNzgzMDUsImp0aSI6InY3eERlMEVTeDB1Y0U3cVciLCJzdWIiOjIzLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.Xut2_kgXBZhfFoFp_UUz1AgGhGxRr_uDY6yuNaf975g";

        $response = $client->get($url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ]
        ]);

        $body = $response->getBody()->getContents();

        $records = json_decode($body, true);
        // dd($records);
        // exit;
        return view('master.dokter.list', compact('records', 'token'));
    }

    public function create()
    {
        return view('master.dokter.form');
    }

    public function store(Request $request)
    {
        $client = new Client();
        $url = "https://api.app-sandbox.xyz/api/dokter/create";
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLmFwcC1zYW5kYm94Lnh5elwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY2NDA3ODMwNSwiZXhwIjoxNjY2NjcwMzA1LCJuYmYiOjE2NjQwNzgzMDUsImp0aSI6InY3eERlMEVTeDB1Y0U3cVciLCJzdWIiOjIzLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.Xut2_kgXBZhfFoFp_UUz1AgGhGxRr_uDY6yuNaf975g";

        try {
            $response = $client->post($url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ],
                'form_params' => [
                    'nm_dokter' => $request->nmDokter,
                ]
            ]);

            $msg = json_decode($response->getBody()->getContents());

            if ($msg->status == 1) {
                return redirect()->route('dokter.index')->with('alert-success', 'Data Berhasil Disimpan.');
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                if ($e->getResponse()->getStatusCode() == '400') {
                    return redirect()->route('dokter.index')->with('alert-error', 'Data Gagal Disimpan.');
                }
            }
        }
    }
    public function edit($id)
    {
        $client = new Client();
        $url = "https://api.app-sandbox.xyz/api/dokter/get/" . $id;
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLmFwcC1zYW5kYm94Lnh5elwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY2NDA3ODMwNSwiZXhwIjoxNjY2NjcwMzA1LCJuYmYiOjE2NjQwNzgzMDUsImp0aSI6InY3eERlMEVTeDB1Y0U3cVciLCJzdWIiOjIzLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.Xut2_kgXBZhfFoFp_UUz1AgGhGxRr_uDY6yuNaf975g";

        $response = $client->get($url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ]
        ]);

        $body = $response->getBody()->getContents();

        $arrData = json_decode($body, true);

        $record = [];
        foreach ($arrData['data'] as $row) {
            $record = ([
                'id_dokter' => $row['id_dokter'],
                'nm_dokter' => $row['nm_dokter']
            ]);
        }

        return view('master.dokter.form', compact('record'));
    }

    public function update(Request $request, $id)
    {
        $client = new Client();
        $url = "https://api.app-sandbox.xyz/api/dokter/update/" . $id;
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY2MzU0NTc5OSwiZXhwIjoxNjY2MTM3Nzk5LCJuYmYiOjE2NjM1NDU3OTksImp0aSI6IkVlOFk0YUlQR3hmbGV6TzMiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.cyyEDMrlWQlX0Z3NGqIlBBWRx4xxL_Cc09QX7ibxXww";


        try {
            $response = $client->put($url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ],
                'form_params' => [
                    'nm_dokter' => $request->nmDokter
                ]
            ]);

            $msg = json_decode($response->getBody()->getContents());

            if ($msg->status == 1) {
                return redirect()->route('dokter.index')->with('alert-success', 'Data Berhasil Disimpan.');
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                if ($e->getResponse()->getStatusCode() == '400') {
                    return redirect()->route('dokter.index')->with('alert-error', 'Data Gagal Disimpan.');
                }
            }
        }
    }

    public function destroy($id): JsonResponse
    {

        $client = new Client();
        $url = "https://api.app-sandbox.xyz/api/dokter/delete/" . $id;
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY2MzU0NTc5OSwiZXhwIjoxNjY2MTM3Nzk5LCJuYmYiOjE2NjM1NDU3OTksImp0aSI6IkVlOFk0YUlQR3hmbGV6TzMiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.cyyEDMrlWQlX0Z3NGqIlBBWRx4xxL_Cc09QX7ibxXww";

        $response = $client->delete($url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ]
        ]);

        $msg = json_decode($response->getBody()->getContents());

        if ($msg->status == 1) {
            $res['status'] = 'success';
        }

        return response()->json($res);
    }
}
