<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class AreaController extends Controller
{
    public function index()
    {
        // $token = Session::get('access_token');
        // exit;
        $client = new Client(); 
        $url = "https://api.app-sandbox.xyz/api/area";
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLmFwcC1zYW5kYm94Lnh5elwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY2Mzk0MTkwOSwiZXhwIjoxNjY2NTMzOTA5LCJuYmYiOjE2NjM5NDE5MDksImp0aSI6InBpN0U1eUl3a05pY1E4cHciLCJzdWIiOjIzLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.MsaX_zqYLjPvKmER9LPMqAQgAq-lc84IGjvI2V9cePI";

        $response = $client->get($url, [
            'headers'=> [
                'Authorization' => 'Bearer '.$token
            ]
        ]);

        $body = $response->getBody()->getContents();

        $records = json_decode($body, true);

        return view('master.area.list', compact('records', 'token'));
    }

    public function create()
    {
        return view('master.area.form');
    }

    public function store(Request $request)
    {
        $client = new Client(); 
        $url = "https://api.app-sandbox.xyz/api/area/create";
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLmFwcC1zYW5kYm94Lnh5elwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY2MzkzNDgwMywiZXhwIjoxNjY2NTI2ODAzLCJuYmYiOjE2NjM5MzQ4MDMsImp0aSI6IjE4TUZsSFplVkhNd0d0dG4iLCJzdWIiOjIzLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.yQ8IMlermvzWpNOBhqR1iNSYxa3CILFiEJQGy1gG5GU";

        try {
            $response = $client->post($url, [
                'headers'=> [
                    'Authorization' => 'Bearer '.$token
                ],
                'form_params' => [
                    'nmArea' => $request->nmArea
                ]
            ]);

            $msg = json_decode($response->getBody()->getContents());

            if ($msg->status == 1){
                return redirect()->route('area.index')->with('alert-success', 'Data Berhasil Disimpan.');
            }

        } catch (RequestException $e) {
            if ($e->hasResponse()){
                if ($e->getResponse()->getStatusCode() == '400') {
                    return redirect()->route('area.index')->with('alert-error', 'Data Gagal Disimpan.');
                }
            }
        }
    }

    public function edit($id)
    {
        $client = new Client(); 
        $url = "https://api.app-sandbox.xyz/api/area/get/".$id;
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLmFwcC1zYW5kYm94Lnh5elwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY2MzkzNDgwMywiZXhwIjoxNjY2NTI2ODAzLCJuYmYiOjE2NjM5MzQ4MDMsImp0aSI6IjE4TUZsSFplVkhNd0d0dG4iLCJzdWIiOjIzLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.yQ8IMlermvzWpNOBhqR1iNSYxa3CILFiEJQGy1gG5GU";

        $response = $client->get($url, [
            'headers'=> [
                'Authorization' => 'Bearer '.$token
            ]
        ]);

        $body = $response->getBody()->getContents();

        $arrData = json_decode($body, true);

        $record = [];
        foreach($arrData['data'] as $row){
            $record = ([
                'id_area' => $row['id_area'],
                'nm_area' => $row['nm_area']
            ]);
        }

        return view('master.area.form', compact('record'));
    }

    public function update(Request $request, $id)
    {
        $client = new Client(); 
        $url = "https://api.app-sandbox.xyz/api/area/update/".$id;
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLmFwcC1zYW5kYm94Lnh5elwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY2MzkzNDgwMywiZXhwIjoxNjY2NTI2ODAzLCJuYmYiOjE2NjM5MzQ4MDMsImp0aSI6IjE4TUZsSFplVkhNd0d0dG4iLCJzdWIiOjIzLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.yQ8IMlermvzWpNOBhqR1iNSYxa3CILFiEJQGy1gG5GU";

        try {
            $response = $client->put($url, [
                'headers'=> [
                    'Authorization' => 'Bearer '.$token
                ],
                'form_params' => [
                    'nmArea' => $request->nmArea
                ]
            ]);

            $msg = json_decode($response->getBody()->getContents());

            if ($msg->status == 1){
                return redirect()->route('area.index')->with('alert-success', 'Data Berhasil Disimpan.');
            }

        } catch (RequestException $e) {
            if ($e->hasResponse()){
                if ($e->getResponse()->getStatusCode() == '400') {
                    return redirect()->route('area.index')->with('alert-error', 'Data Gagal Disimpan.');
                }
            }
        }
    }

    public function destroy($id): JsonResponse
    {
        $client = new Client(); 
        $url = "https://api.app-sandbox.xyz/api/area/delete/".$id;
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLmFwcC1zYW5kYm94Lnh5elwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY2MzkzNDgwMywiZXhwIjoxNjY2NTI2ODAzLCJuYmYiOjE2NjM5MzQ4MDMsImp0aSI6IjE4TUZsSFplVkhNd0d0dG4iLCJzdWIiOjIzLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.yQ8IMlermvzWpNOBhqR1iNSYxa3CILFiEJQGy1gG5GU";

        $response = $client->delete($url, [
            'headers'=> [
                'Authorization' => 'Bearer '.$token
            ]
        ]);

        $msg = json_decode($response->getBody()->getContents());

        if ($msg->status == 1){
            $res['status'] = 'success';
        }

        return response()->json($res);
    }
}
