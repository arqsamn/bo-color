<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class BarangBaruController extends Controller
{
    public function index()
    {
        return view('master.usulan_barang_baru.list');
    }

    public function create()
    {
        return view('master.usulan_barang_baru.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('master.usulan_barang_baru.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
