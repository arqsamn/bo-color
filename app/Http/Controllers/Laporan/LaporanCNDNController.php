<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LaporanCNDNController extends Controller
{
    public function index()
    {
        return view('laporan.laporan_cn_dn.list');
    }

    public function create()
    {
        return view('laporan.laporan_cn_dn.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.laporan_cn_dn.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
