<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LaporanAntrianResepController extends Controller
{
    public function index()
    {
        return view('laporan.laporan_antrian_resep.list');
    }

    public function create()
    {
        return view('laporan.laporan_antrian_resep.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.laporan_antrian_resep.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
