<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LaporanPenjualanController extends Controller
{
    public function index()
    {
        return view('laporan.laporan_penjualan.list');
    }

    public function create()
    {
        return view('laporan.laporan_penjualan.form');
    }

    public function store()
    {
        return view('laporan.laporan_penjualan.print');
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.laporan_penjualan.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
    public function print()
    {
        return view('laporan.laporan_penjualan.print');
    }
}
