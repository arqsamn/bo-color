<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LaporanPembelianBarangQTYController extends Controller
{
    public function index()
    {
        return view('laporan.laporan_pembelian_barang_qty.list');
    }

    public function create()
    {
        return view('laporan.laporan_pembelian_barang_qty.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.laporan_pembelian_barang_qty.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }

    public function print()
    {
        return view('laporan.laporan_pembelian_barang_qty.print');
    }
}
