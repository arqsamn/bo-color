<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LaporanPembelianController extends Controller
{
    public function index()
    {
        return view('laporan.laporan_pembelian.list');
    }

    public function create()
    {
        return view('laporan.laporan_pembelian.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.laporan_pembelian.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
