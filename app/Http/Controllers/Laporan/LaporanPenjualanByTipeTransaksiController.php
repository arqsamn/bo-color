<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LaporanPenjualanByTipeTransaksiController extends Controller
{
    public function index()
    {
        return view('laporan.laporan_penjualan_transaksi.list');
    }

    public function create()
    {
        return view('laporan.laporan_penjualan_transaksi.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.laporan_penjualan_transaksi.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }

    public function print_resep()
    {
        return view('laporan.laporan_penjualan_transaksi.print_resep');
    }

    public function print_swalayan()
    {
        return view('laporan.laporan_penjualan_transaksi.print_swalayan');
    }

}
