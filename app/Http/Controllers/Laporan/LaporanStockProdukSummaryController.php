<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LaporanStockProdukSummaryController extends Controller
{
    public function index()
    {
        return view('laporan.laporan_stock_summary.list');
    }

    public function create()
    {
        return view('laporan.laporan_stock_summary.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.laporan_stock_summary.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
