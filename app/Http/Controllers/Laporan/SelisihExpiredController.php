<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class SelisihExpiredController extends Controller
{
    public function index()
    {
        return view('laporan.selisih_expired.list');
    }

    public function create()
    {
        return view('laporan.selisih_expired.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.selisih_expired.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
