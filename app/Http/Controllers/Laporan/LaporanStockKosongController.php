<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LaporanStockKosongController extends Controller
{
    public function index()
    {
        return view('laporan.laporan_stock_kosong.list');
    }

    public function create()
    {
        return view('laporan.laporan_stock_kosong.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.laporan_stock_kosong.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
