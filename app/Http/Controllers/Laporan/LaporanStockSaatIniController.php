<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LaporanStockSaatIniController extends Controller
{
    public function index()
    {
        return view('laporan.laporan_stock_saat_ini.list');
    }

    public function create()
    {
        return view('laporan.laporan_stock_saat_ini.form');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.laporan_stock_saat_ini.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
