<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class LaporanPenjualanDalamPeriodeController extends Controller
{
    public function index()
    {
        return view('laporan.laporan_penjualan_dalam_periode.list');
    }

    public function create()
    {
        return view('laporan.laporan_penjualan_dalam_periode.form');
    }

    public function print()
    {
        return view('laporan.laporan_penjualan_dalam_periode.print');
    }

    public function store()
    {
    }

    public function edit($id)
    {
        $data['record'] = $id;
        return view('laporan.laporan_penjualan_dalam_periode.form', $data);
    }

    public function update()
    {
    }

    public function destroy($id): JsonResponse
    {
    }
}
