<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $client = new Client(); 
        $url = "https://api.app-sandbox.xyz/api/auth/login";

        try {
            $response = $client->post($url, [
                'form_params' => [
                    'email' => $request->email,
                    'password' => $request->password
                ]
            ]);

            $auth = json_decode($response->getBody()->getContents());

            if ($auth->status == 1){
            	$this->setUserSession($auth);
                // $user = Auth::user($auth);
                return redirect('home');
            }
        } catch (RequestException $e) {
            if ($e->hasResponse()){
                if ($e->getResponse()->getStatusCode() == '401') {
                   return redirect('/');
                }
            }
        }
    }

    public function logout()
    {
        Session::forget('access_token');
        return redirect()->route('login');
    }

    protected function setUserSession($auth)
	{
	    session([
	            'access_token' => $auth->data->access_token,
	        	'name' => $auth->data->user->name,
	        	'email' => $auth->data->user->email
        ]);
	}
}
